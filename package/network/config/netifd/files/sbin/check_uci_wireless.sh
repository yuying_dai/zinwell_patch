#!/bin/sh
# Copyright (C) 2018 Zinwell

wifimode2g=`uci get wireless.mt7615e2.wifimode`
wifimode5g=`uci get wireless.mt7615e5.wifimode`

if [ "$wifimode2g" = "" -o "$wifimode5g" = "" ]; then
    echo "load uci wireless default"
    #2.4G radio default settings
    uci set wireless.mt7615e2.radio=1
    uci set wireless.mt7615e2.wifimode=9
    uci set wireless.mt7615e2.channel=6
    uci set wireless.mt7615e2.autoch=0
    uci set wireless.mt7615e2.bw=0
    uci set wireless.mt7615e2.country=US
    uci set wireless.mt7615e2.aregion=13
    uci set wireless.mt7615e2.bgprotect=0
    uci set wireless.mt7615e2.beacon=100
    uci set wireless.mt7615e2.dtim=1
    uci set wireless.mt7615e2.fragthres=2346
    uci set wireless.mt7615e2.rtsthres=2347
    uci set wireless.mt7615e2.txpower=100
    uci set wireless.mt7615e2.txpreamble=0
    uci set wireless.mt7615e2.shortslot=1
    uci set wireless.mt7615e2.txburst=1
    uci set wireless.mt7615e2.pktaggre=1
    uci set wireless.mt7615e2.ieee80211h=0
    uci set wireless.mt7615e2.txbf=2
    uci set wireless.mt7615e2.region=0
    uci set wireless.mt7615e2.rdregion=FCC
    uci set wireless.mt7615e2.dfsen=0
    uci set wireless.mt7615e2.rrmenable=1
    uci set wireless.mt7615e2.ht_bsscoexist=0
    uci set wireless.mt7615e2.ht_opmode=0
    uci set wireless.mt7615e2.ht_gi=1
    uci set wireless.mt7615e2.ht_rdg=1
    uci set wireless.mt7615e2.ht_stbc=1
    uci set wireless.mt7615e2.ht_amsdu=1
    uci set wireless.mt7615e2.ht_autoba=1
    uci set wireless.mt7615e2.ht_badec=0
    uci set wireless.mt7615e2.ht_distkip=1
    uci set wireless.mt7615e2.ht_ldpc=1
    uci set wireless.mt7615e2.vht_stbc=0
    uci set wireless.mt7615e2.vht_sgi=0
    uci set wireless.mt7615e2.vht_bw_sig=0
    uci set wireless.mt7615e2.vht_ldpc=0
    uci set wireless.mt7615e2.ht_txstream=2
    uci set wireless.mt7615e2.ht_rxstream=2

    #5G radio default settings
    uci set wireless.mt7615e5.radio=1
    uci set wireless.mt7615e5.wifimode=14
    uci set wireless.mt7615e5.channel=36
    uci set wireless.mt7615e5.autoch=0
    uci set wireless.mt7615e5.bw=2
    uci set wireless.mt7615e5.country=US
    uci set wireless.mt7615e5.aregion=13
    uci set wireless.mt7615e5.bgprotect=0
    uci set wireless.mt7615e5.beacon=100
    uci set wireless.mt7615e5.dtim=1
    uci set wireless.mt7615e5.fragthres=2346
    uci set wireless.mt7615e5.rtsthres=2347
    uci set wireless.mt7615e5.txpower=100
    uci set wireless.mt7615e5.txpreamble=1
    uci set wireless.mt7615e5.shortslot=1
    uci set wireless.mt7615e5.txburst=1
    uci set wireless.mt7615e5.pktaggre=1
    uci set wireless.mt7615e5.ieee80211h=1
    uci set wireless.mt7615e5.txbf=2
    uci set wireless.mt7615e5.region=0
    uci set wireless.mt7615e5.rdregion=FCC
    uci set wireless.mt7615e5.dfsen=1
    uci set wireless.mt7615e5.rrmenable=1
    uci set wireless.mt7615e5.ht_bsscoexist=0
    uci set wireless.mt7615e5.ht_opmode=0
    uci set wireless.mt7615e5.ht_gi=1
    uci set wireless.mt7615e5.ht_rdg=1
    uci set wireless.mt7615e5.ht_stbc=1
    uci set wireless.mt7615e5.ht_amsdu=1
    uci set wireless.mt7615e5.ht_autoba=1
    uci set wireless.mt7615e5.ht_badec=0
    uci set wireless.mt7615e5.ht_distkip=1
    uci set wireless.mt7615e5.ht_ldpc=1
    uci set wireless.mt7615e5.vht_stbc=1
    uci set wireless.mt7615e5.vht_sgi=1
    uci set wireless.mt7615e5.vht_bw_sig=0
    uci set wireless.mt7615e5.vht_ldpc=1
    uci set wireless.mt7615e5.ht_txstream=2
    uci set wireless.mt7615e5.ht_rxstream=2

    i=0
    j=0
    BssidNum=4
	IFACE_2G_SSID=Verizon2G
	IFACE_5G_SSID=Verizon5G
	
    while [ $i -lt $BssidNum ]
    do    
        IFACE_2IDX=$j
        IFACE_5IDX=`expr $j + 1`
        IFACE_2G_NAME=rax$i
        IFACE_5G_NAME=ra$i
        
		if [ $i -gt 0 ]; then
            IFACE_2G_SSID=Verizon2G$i
            IFACE_5G_SSID=Verizon5G$i
		fi
        
        j=`expr $j + 2`
        i=`expr $i + 1`

        IFACE_SECTION=""
        IFACE_SECTION=`uci get wireless.@wifi-iface[$IFACE_2IDX]`
        if [ "$IFACE_SECTION" = "" ]; then
            uci add wireless wifi-iface
        fi

        #wifi-iface[0,2,4,6] 2.4G SSID
        uci set wireless.@wifi-iface[$IFACE_2IDX]=wifi-iface
        uci set wireless.@wifi-iface[$IFACE_2IDX].device=mt7615e2
        uci set wireless.@wifi-iface[$IFACE_2IDX].network=lan
        uci set wireless.@wifi-iface[$IFACE_2IDX].mode=ap
        uci set wireless.@wifi-iface[$IFACE_2IDX].ifname=$IFACE_2G_NAME
        uci set wireless.@wifi-iface[$IFACE_2IDX].wmm=1
        uci set wireless.@wifi-iface[$IFACE_2IDX].apsd=0
        uci set wireless.@wifi-iface[$IFACE_2IDX].rekeyinteval=3600
        uci set wireless.@wifi-iface[$IFACE_2IDX].ssid=$IFACE_2G_SSID
        uci set wireless.@wifi-iface[$IFACE_2IDX].encryption=psk2+ccmp
        uci set wireless.@wifi-iface[$IFACE_2IDX].key=12345678

        IFACE_SECTION=""
        IFACE_SECTION=`uci get wireless.@wifi-iface[$IFACE_5IDX]`
        if [ "$IFACE_SECTION" = "" ]; then
            uci add wireless wifi-iface
        fi

        #wifi-iface[1,3,5,7] 5G SSID
        uci set wireless.@wifi-iface[$IFACE_5IDX]=wifi-iface
        uci set wireless.@wifi-iface[$IFACE_5IDX].device=mt7615e5
        uci set wireless.@wifi-iface[$IFACE_5IDX].network=lan
        uci set wireless.@wifi-iface[$IFACE_5IDX].mode=ap
        uci set wireless.@wifi-iface[$IFACE_5IDX].ifname=$IFACE_5G_NAME
        uci set wireless.@wifi-iface[$IFACE_5IDX].wmm=1
        uci set wireless.@wifi-iface[$IFACE_5IDX].apsd=0
        uci set wireless.@wifi-iface[$IFACE_5IDX].rekeyinteval=3600
        uci set wireless.@wifi-iface[$IFACE_5IDX].ssid=$IFACE_5G_SSID
        uci set wireless.@wifi-iface[$IFACE_5IDX].encryption=psk2+ccmp
        uci set wireless.@wifi-iface[$IFACE_5IDX].key=12345678
    done
    
    uci commit wireless
fi
