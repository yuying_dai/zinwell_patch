#!/bin/sh
# Copyright (C) 2018 Zinwell

basic_config()
{
    #5g basic
    RadioOn_5g=`uci get wireless.$UCI_DEV.radio`
    WirelessMode_5g=`uci get wireless.$UCI_DEV.wifimode`
    Channel_5g=`uci get wireless.$UCI_DEV.channel`
    AutoChannelSelect_5g=`uci get wireless.$UCI_DEV.autoch`
    bw_5g=`uci get wireless.$UCI_DEV.bw`
    
    nvram set $NVRAM_SECTION RadioOn $RadioOn_5g
    nvram set $NVRAM_SECTION WirelessMode $WirelessMode_5g
    nvram set $NVRAM_SECTION Channel $Channel_5g
    nvram set $NVRAM_SECTION AutoChannelSelect $AutoChannelSelect_5g
 
# ugly fix, assumes BssidNum=4 
    if [ "$bw_5g" = "2" ]; then
        nvram set $NVRAM_SECTION HT_BW "1;1;1;1"
        nvram set $NVRAM_SECTION VHT_BW 1
    elif [ "$bw_5g" = "1" ]; then
        nvram set $NVRAM_SECTION HT_BW "1;1;1;1"
        nvram set $NVRAM_SECTION VHT_BW 0
    else
        nvram set $NVRAM_SECTION HT_BW "0;0;0;0"
        nvram set $NVRAM_SECTION VHT_BW 0
    fi

}

advanced_config()
{
    #5g advanced
    CountryCode_5g=`uci get wireless.$UCI_DEV.country`
    CountryRegionABand_5g=`uci get wireless.$UCI_DEV.aregion`
    BGProtection_5g=`uci get wireless.$UCI_DEV.bgprotect`
    BeaconPeriod_5g=`uci get wireless.$UCI_DEV.beacon`
    DtimPeriod_5g=`uci get wireless.$UCI_DEV.dtim`
    FragThreshold_5g=`uci get wireless.$UCI_DEV.fragthres`
    RTSThreshold_5g=`uci get wireless.$UCI_DEV.rtsthres`
    TxPower_5g=`uci get wireless.$UCI_DEV.txpower`
    TxPreamble_5g=`uci get wireless.$UCI_DEV.txpreamble`
    ShortSlot_5g=`uci get wireless.$UCI_DEV.shortslot`
    TxBurst_5g=`uci get wireless.$UCI_DEV.txburst`
    PktAggregate_5g=`uci get wireless.$UCI_DEV.pktaggre`
    IEEE80211H_5g=`uci get wireless.$UCI_DEV.ieee80211h`
    txbf_5g=`uci get wireless.$UCI_DEV.txbf`
	#CountryRegion_5g=`uci get wireless.$UCI_DEV.region`
	#RDRegion_5g=`uci get wireless.$UCI_DEV.rdregion`
	#DfsEnable_5g=`uci get wireless.$UCI_DEV.dfsen`
    #uci get wireless.$UCI_DEV.igmpsnoop=$
    
    nvram set $NVRAM_SECTION CountryCode $CountryCode_5g
    nvram set $NVRAM_SECTION CountryRegionABand $CountryRegionABand_5g
    nvram set $NVRAM_SECTION BGProtection $BGProtection_5g
    nvram set $NVRAM_SECTION BeaconPeriod $BeaconPeriod_5g
    nvram set $NVRAM_SECTION DtimPeriod $DtimPeriod_5g
    nvram set $NVRAM_SECTION FragThreshold $FragThreshold_5g
    nvram set $NVRAM_SECTION RTSThreshold $RTSThreshold_5g
    nvram set $NVRAM_SECTION TxPower $TxPower_5g
    nvram set $NVRAM_SECTION TxPreamble $TxPreamble_5g
    nvram set $NVRAM_SECTION ShortSlot $ShortSlot_5g
    nvram set $NVRAM_SECTION TxBurst $TxBurst_5g
    nvram set $NVRAM_SECTION PktAggregate $PktAggregate_5g
    nvram set $NVRAM_SECTION IEEE80211H $IEEE80211H_5g
    #nvram set $NVRAM_SECTION ETxBfEnCond $ETxBfEnCond_5g
    #if [ "$txbf_5g" = "1" ]; then
    #    nvram set $NVRAM_SECTION ITxBfEn 1
    #    nvram set $NVRAM_SECTION ETxBfEnCond 0
    #    nvram set $NVRAM_SECTION MUTxRxEnable 0
    #elif [ "$txbf_5g" = "2" ]; then
        nvram set $NVRAM_SECTION ITxBfEn 0
        nvram set $NVRAM_SECTION ETxBfEnCond 1
        nvram set $NVRAM_SECTION MUTxRxEnable 0
    #elif [ "$txbf_5g" = "0" ]; then
    #    nvram set $NVRAM_SECTION ITxBfEn 0
    #    nvram set $NVRAM_SECTION ETxBfEnCond 0
    #    nvram set $NVRAM_SECTION MUTxRxEnable 0
    #else
    #    nvram set $NVRAM_SECTION ITxBfEn 1
    #    nvram set $NVRAM_SECTION MUTxRxEnable 0
    #    nvram set $NVRAM_SECTION ETxBfEnCond 1
    #fi
    #nvram set $NVRAM_SECTION igmpEnabled 1
	#CountryRegion, RDRegion, and DfsEnable not write back to nvram
	#nvram set $NVRAM_SECTION CountryRegion $CountryRegion_5g
	#nvram set $NVRAM_SECTION RDRegion $RDRegion_5g
	#nvram set $NVRAM_SECTION DfsEnable $DfsEnable_5g
}

ht_config()
{
    #5g HT
    HT_BSSCoexistence_5g=`uci get wireless.mt7615e5.ht_bsscoexist`
    HT_OpMode_5g=`uci get wireless.mt7615e5.ht_opmode`
    HT_GI_5g=`uci get wireless.mt7615e5.ht_gi`
    HT_RDG_5g=`uci get wireless.mt7615e5.ht_rdg`
    HT_STBC_5g=`uci get wireless.mt7615e5.ht_stbc`
    HT_AMSDU_5g=`uci get wireless.mt7615e5.ht_amsdu`
    HT_AutoBA_5g=`uci get wireless.mt7615e5.ht_autoba`
    HT_BADecline_5g=`uci get wireless.mt7615e5.ht_badec`
    HT_DisallowTKIP_5g=`uci get wireless.mt7615e5.ht_distkip`
    HT_LDPC_5g=`uci get wireless.mt7615e5.ht_ldpc`
    VHT_STBC_5g=`uci get wireless.mt7615e5.vht_stbc`
    VHT_SGI_5g=`uci get wireless.mt7615e5.vht_sgi`
    VHT_BW_SIGNAL_5g=`uci get wireless.mt7615e5.vht_bw_sig`
    VHT_LDPC_5g=`uci get wireless.mt7615e5.vht_ldpc`
    HT_TxStream_5g=`uci get wireless.mt7615e5.ht_txstream`
    HT_RxStream_5g=`uci get wireless.mt7615e5.ht_rxstream`
    
    nvram set test1 HT_BSSCoexistence $HT_BSSCoexistence_5g
    nvram set test1 HT_OpMode $HT_OpMode_5g
    nvram set test1 HT_GI $HT_GI_5g
    nvram set test1 HT_RDG $HT_RDG_5g
    nvram set test1 HT_STBC $HT_STBC_5g
    nvram set test1 HT_AMSDU $HT_AMSDU_5g
    nvram set test1 HT_AutoBA $HT_AutoBA_5g
    nvram set test1 HT_BADecline $HT_BADecline_5g
    nvram set test1 HT_DisallowTKIP $HT_DisallowTKIP_5g
    nvram set test1 HT_LDPC $HT_LDPC_5g
    nvram set test1 VHT_STBC $VHT_STBC_5g
    nvram set test1 VHT_SGI $VHT_SGI_5g
    nvram set test1 VHT_BW_SIGNAL $VHT_BW_SIGNAL_5g
    nvram set test1 VHT_LDPC $VHT_LDPC_5g
    nvram set test1 HT_TxStream $HT_TxStream_5g
    nvram set test1 HT_RxStream $HT_RxStream_5g
}

IFACE_5G_COUNT=0
IFACE_2G_COUNT=0
IFACE_MAX=16

mbssid_config_all()
{
    WMMCAPABLE5G=""
    WMMCAPABLE2G=""
    APSDCAPABLE5G=""
    APSDCAPABLE2G=""
    REKEYINTERVAL5G=""
    REKEYINTERVAL2G=""
    i=0
    IFACE_NAME=""
    
    while [ $i -lt $IFACE_MAX ]
    do
        IFACE_NAME="@wifi-iface[$i]"
        DEVNAME=""
        DEVNAME=`uci get wireless.$IFACE_NAME.device`
        #echo $DEVNAME
        if [ "$DEVNAME" = "mt7615e5" ]; then
            #5G
            IFACE_5G_COUNT=`expr $IFACE_5G_COUNT + 1`
            if [ "$WMMCAPABLE5G" = "" ]; then
                WMMCAPABLE5G=`uci get wireless.$IFACE_NAME.wmm`
                nvram set test1 WmmCapable $WMMCAPABLE5G
            fi
            if [ "$APSDCAPABLE5G" = "" ]; then
                APSDCAPABLE5G=`uci get wireless.$IFACE_NAME.apsd`
                nvram set test1 APSDCapable $APSDCAPABLE5G
            fi
            if [ "$REKEYINTERVAL5G" = "" ]; then
                REKEYINTERVAL5G=`uci get wireless.$IFACE_NAME.rekeyinteval`
                nvram set test1 RekeyInterval $REKEYINTERVAL5G
            fi
            SSID=`uci get wireless.$IFACE_NAME.ssid`
            nvram set test1 SSID$IFACE_5G_COUNT "$SSID"
            
            WPAPSK=`uci get wireless.$IFACE_NAME.key`
            nvram set test1 WPAPSK$IFACE_5G_COUNT "$WPAPSK"
            
            ENCRYPTYPE_5G=""
            ENCRYPTYPE_5G=`uci get wireless.$IFACE_NAME.encryption`
            #echo $ENCRYPTYPE_5G
            if [ "$IFACE_5G_COUNT" = "1" ]; then
                if [ "$ENCRYPTYPE_5G" = "psk2+ccmp" ]; then
                    AuthMode5g="WPA2PSK"
                    EncrypType5g="AES"
                elif [ "$ENCRYPTYPE_5G" = "psk2" ]; then
                    AuthMode5g="WPA2PSK"
                    EncrypType5g="AES"
                elif [ "$ENCRYPTYPE_5G" = "psk-mixed+ccmp" ]; then
                    AuthMode5g="WPAPSKWPA2PSK"
                    EncrypType5g="AES"
                elif [ "$ENCRYPTYPE_5G" = "psk-mixed+tkip+ccmp" ]; then
                    AuthMode5g="WPAPSKWPA2PSK"
                    EncrypType5g="TKIPAES"
                elif [ "$ENCRYPTYPE_5G" = "psk2+tkip+ccmp" ]; then
                    AuthMode5g="WPA2PSK"
                    EncrypType5g="TKIPAES"
                elif [ "$ENCRYPTYPE_5G" = "none" ]; then
                    AuthMode5g="OPEN"
                    EncrypType5g="NONE"
                fi
            else
                if [ "$ENCRYPTYPE_5G" = "psk2+ccmp" ]; then
                    AuthMode5g="$AuthMode5g;WPA2PSK"
                    EncrypType5g="$EncrypType5g;AES"
                elif [ "$ENCRYPTYPE_5G" = "psk-mixed+ccmp" ]; then
                    AuthMode5g="$AuthMode5g;WPAPSKWPA2PSK"
                    EncrypType5g="$EncrypType5g;AES"
                elif [ "$ENCRYPTYPE_5G" = "psk-mixed+tkip+ccmp" ]; then
                    AuthMode5g="$AuthMode5g;WPAPSKWPA2PSK"
                    EncrypType5g="$EncrypType5g;TKIPAES"
                elif [ "$ENCRYPTYPE_5G" = "psk2+tkip+ccmp" ]; then
                    AuthMode5g="$AuthMode5g;WPA2PSK"
                    EncrypType5g="$EncrypType5g;TKIPAES"
                elif [ "$ENCRYPTYPE_5G" = "none" ]; then
                    AuthMode5g="$AuthMode5g;OPEN"
                    EncrypType5g="$EncrypType5g;NONE"
                fi            
            fi
            
        elif [ "$DEVNAME" = "mt7615e2" ]; then
            #2.4G
            IFACE_2G_COUNT=`expr $IFACE_2G_COUNT + 1`
            if [ "$WMMCAPABLE2G" = "" ]; then
                WMMCAPABLE2G=`uci get wireless.$IFACE_NAME.wmm`
                nvram set test3 WmmCapable $WMMCAPABLE2G
            fi
            if [ "$APSDCAPABLE2G" = "" ]; then
                APSDCAPABLE2G=`uci get wireless.$IFACE_NAME.apsd`
                nvram set test3 APSDCapable $APSDCAPABLE2G
            fi
            if [ "$REKEYINTERVAL2G" = "" ]; then
                REKEYINTERVAL2G=`uci get wireless.$IFACE_NAME.rekeyinteval`
                nvram set test3 RekeyInterval $REKEYINTERVAL2G
            fi
            SSID=`uci get wireless.$IFACE_NAME.ssid`
            nvram set test3 SSID$IFACE_2G_COUNT "$SSID"
            
            WPAPSK=`uci get wireless.$IFACE_NAME.key`
            nvram set test3 WPAPSK$IFACE_2G_COUNT "$WPAPSK"
            
            ENCRYPTYPE_2G=""
            ENCRYPTYPE_2G=`uci get wireless.$IFACE_NAME.encryption`
            #echo $ENCRYPTYPE_2G
            if [ "$IFACE_2G_COUNT" = "1" ]; then
                if [ "$ENCRYPTYPE_2G" = "psk2+ccmp" ]; then
                    AuthMode2g="WPA2PSK"
                    EncrypType2g="AES"
                elif [ "$ENCRYPTYPE_2G" = "psk2" ]; then
                    AuthMode2g="WPA2PSK"
                    EncrypType2g="AES"
                elif [ "$ENCRYPTYPE_2G" = "psk-mixed+ccmp" ]; then
                    AuthMode2g="WPAPSKWPA2PSK"
                    EncrypType2g="AES"
                elif [ "$ENCRYPTYPE_2G" = "psk-mixed+tkip+ccmp" ]; then
                    AuthMode2g="WPAPSKWPA2PSK"
                    EncrypType2g="TKIPAES"
                elif [ "$ENCRYPTYPE_2G" = "psk2+tkip+ccmp" ]; then
                    AuthMode2g="WPA2PSK"
                    EncrypType2g="TKIPAES"
                elif [ "$ENCRYPTYPE_2G" = "none" ]; then
                    AuthMode2g="OPEN"
                    EncrypType2g="NONE"
                fi
            else
                if [ "$ENCRYPTYPE_2G" = "psk2+ccmp" ]; then
                    AuthMode2g="$AuthMode2g;WPA2PSK"
                    EncrypType2g="$EncrypType2g;AES"
                elif [ "$ENCRYPTYPE_2G" = "psk-mixed+ccmp" ]; then
                    AuthMode2g="$AuthMode2g;WPAPSKWPA2PSK"
                    EncrypType2g="$EncrypType2g;AES"
                elif [ "$ENCRYPTYPE_2G" = "psk-mixed+tkip+ccmp" ]; then
                    AuthMode2g="$AuthMode2g;WPAPSKWPA2PSK"
                    EncrypType2g="$EncrypType2g;TKIPAES"
                elif [ "$ENCRYPTYPE_2G" = "psk2+tkip+ccmp" ]; then
                    AuthMode2g="$AuthMode2g;WPA2PSK"
                    EncrypType2g="$EncrypType2g;TKIPAES"
                elif [ "$ENCRYPTYPE_2G" = "none" ]; then
                    AuthMode2g="$AuthMode2g;OPEN"
                    EncrypType2g="$EncrypType2g;NONE"
                fi            
            fi
        fi
        
        i=`expr $i + 1`
        
    done
    
    nvram set test1 BssidNum $IFACE_5G_COUNT
    nvram set test3 BssidNum $IFACE_2G_COUNT
    
    nvram set test1 AuthMode "$AuthMode5g"
    nvram set test1 EncrypType "$EncrypType5g"
    nvram set test3 AuthMode "$AuthMode2g"
    nvram set test3 EncrypType "$EncrypType2g"
    
}

UCI_DEV="mt7615e5"
NVRAM_SECTION="test1"

basic_config
advanced_config

UCI_DEV="mt7615e2"
NVRAM_SECTION="test3"

basic_config
advanced_config

mbssid_config_all

nvram commit
