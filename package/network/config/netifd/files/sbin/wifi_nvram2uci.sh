#!/bin/sh
# Copyright (C) 2018 Zinwell

basic_config()
{
    #5g basic
    RadioOn_5g=`nvram get $NVRAM_SECTION RadioOn`
    WirelessMode_5g=`nvram get $NVRAM_SECTION WirelessMode`
    Channel_5g=`nvram get $NVRAM_SECTION Channel`
    AutoChannelSelect_5g=`nvram get $NVRAM_SECTION AutoChannelSelect`
    
    HT_BW_5g=`nvram get $NVRAM_SECTION HT_BW`
    VHT_BW_5g=`nvram get $NVRAM_SECTION VHT_BW`
    
    uci set wireless.$UCI_DEV.radio=$RadioOn_5g
    uci set wireless.$UCI_DEV.wifimode=$WirelessMode_5g
    
    if [ "$AutoChannelSelect_5g" == "0" ]; then
        uci set wireless.$UCI_DEV.channel=$Channel_5g
    else
        uci set wireless.$UCI_DEV.channel=0
    fi
    
    uci set wireless.$UCI_DEV.autoch=$AutoChannelSelect_5g
 
# ugly and quick change -- only supports BssidNum=4 per radio 
    if [ "$HT_BW_5g" == "1;1;1;1" ] && [ "$VHT_BW_5g" == "1" ]; then
        uci set wireless.$UCI_DEV.bw=2
    elif [ "$HT_BW_5g" == "0;0;0;0" ] && [ "$VHT_BW_5g" == "0" ]; then
        uci set wireless.$UCI_DEV.bw=0
    else
        uci set wireless.$UCI_DEV.bw=1
    fi

}

advanced_config()
{
    #5g advanced
    CountryCode_5g=`nvram get $NVRAM_SECTION CountryCode`
    CountryRegionABand_5g=`nvram get $NVRAM_SECTION CountryRegionABand`
    BGProtection_5g=`nvram get $NVRAM_SECTION BGProtection`
    BeaconPeriod_5g=`nvram get $NVRAM_SECTION BeaconPeriod`
    DtimPeriod_5g=`nvram get $NVRAM_SECTION DtimPeriod`
    FragThreshold_5g=`nvram get $NVRAM_SECTION FragThreshold`
    RTSThreshold_5g=`nvram get $NVRAM_SECTION RTSThreshold`
    TxPower_5g=`nvram get $NVRAM_SECTION TxPower`
    TxPreamble_5g=`nvram get $NVRAM_SECTION TxPreamble`
    ShortSlot_5g=`nvram get $NVRAM_SECTION ShortSlot`
    TxBurst_5g=`nvram get $NVRAM_SECTION TxBurst`
    PktAggregate_5g=`nvram get $NVRAM_SECTION PktAggregate`
    IEEE80211H_5g=`nvram get $NVRAM_SECTION IEEE80211H`
	CountryRegion_5g=`nvram get $NVRAM_SECTION CountryRegion`
	RDRegion_5g=`nvram get $NVRAM_SECTION RDRegion`
	DfsEnable_5g=`nvram get $NVRAM_SECTION DfsEnable`
	RRMEnable_5g=`nvram get $NVRAM_SECTION RRMEnable`
    
    uci set wireless.$UCI_DEV.country=$CountryCode_5g
    uci set wireless.$UCI_DEV.aregion=$CountryRegionABand_5g
    uci set wireless.$UCI_DEV.bgprotect=$BGProtection_5g
    uci set wireless.$UCI_DEV.beacon=$BeaconPeriod_5g
    uci set wireless.$UCI_DEV.dtim=$DtimPeriod_5g
    uci set wireless.$UCI_DEV.fragthres=$FragThreshold_5g
    uci set wireless.$UCI_DEV.rtsthres=$RTSThreshold_5g
    uci set wireless.$UCI_DEV.txpower=$TxPower_5g
    uci set wireless.$UCI_DEV.txpreamble=$TxPreamble_5g
    uci set wireless.$UCI_DEV.shortslot=$ShortSlot_5g
    uci set wireless.$UCI_DEV.txburst=$TxBurst_5g
    uci set wireless.$UCI_DEV.pktaggre=$PktAggregate_5g
    uci set wireless.$UCI_DEV.ieee80211h=$IEEE80211H_5g
    #force ETxBf    
    uci set wireless.$UCI_DEV.txbf=2
	uci set wireless.$UCI_DEV.region=$CountryRegion_5g
	uci set wireless.$UCI_DEV.rdregion=$RDRegion_5g
	uci set wireless.$UCI_DEV.dfsen=$DfsEnable_5g
	uci set wireless.$UCI_DEV.rrmenable=$RRMEnable_5g
}

ht_config()
{
    #5g HT
    HT_BSSCoexistence_5g=`nvram get $NVRAM_SECTION HT_BSSCoexistence`
    HT_OpMode_5g=`nvram get $NVRAM_SECTION HT_OpMode`
    HT_GI_5g=`nvram get $NVRAM_SECTION HT_GI`
    HT_RDG_5g=`nvram get $NVRAM_SECTION HT_RDG`
    HT_STBC_5g=`nvram get $NVRAM_SECTION HT_STBC`
    HT_AMSDU_5g=`nvram get $NVRAM_SECTION HT_AMSDU`
    HT_AutoBA_5g=`nvram get $NVRAM_SECTION HT_AutoBA`
    HT_BADecline_5g=`nvram get $NVRAM_SECTION HT_BADecline`
    HT_DisallowTKIP_5g=`nvram get $NVRAM_SECTION HT_DisallowTKIP`
    HT_LDPC_5g=`nvram get $NVRAM_SECTION HT_LDPC`
    VHT_STBC_5g=`nvram get $NVRAM_SECTION VHT_STBC`
    VHT_SGI_5g=`nvram get $NVRAM_SECTION VHT_SGI`
    VHT_BW_SIGNAL_5g=`nvram get $NVRAM_SECTION VHT_BW_SIGNAL`
    VHT_LDPC_5g=`nvram get $NVRAM_SECTION VHT_LDPC`
    HT_TxStream_5g=`nvram get $NVRAM_SECTION HT_TxStream`
    HT_RxStream_5g=`nvram get $NVRAM_SECTION HT_RxStream`
    
    uci set wireless.$UCI_DEV.ht_bsscoexist=$HT_BSSCoexistence_5g
    uci set wireless.$UCI_DEV.ht_opmode=$HT_OpMode_5g
    uci set wireless.$UCI_DEV.ht_gi=$HT_GI_5g
    uci set wireless.$UCI_DEV.ht_rdg=$HT_RDG_5g
    uci set wireless.$UCI_DEV.ht_stbc=$HT_STBC_5g
    uci set wireless.$UCI_DEV.ht_amsdu=$HT_AMSDU_5g
    uci set wireless.$UCI_DEV.ht_autoba=$HT_AutoBA_5g
    uci set wireless.$UCI_DEV.ht_badec=$HT_BADecline_5g
    uci set wireless.$UCI_DEV.ht_distkip=$HT_DisallowTKIP_5g
    uci set wireless.$UCI_DEV.ht_ldpc=$HT_LDPC_5g
    uci set wireless.$UCI_DEV.vht_stbc=$VHT_STBC_5g
    uci set wireless.$UCI_DEV.vht_sgi=$VHT_SGI_5g
    uci set wireless.$UCI_DEV.vht_bw_sig=$VHT_BW_SIGNAL_5g
    uci set wireless.$UCI_DEV.vht_ldpc=$VHT_LDPC_5g
    uci set wireless.$UCI_DEV.ht_txstream=$HT_TxStream_5g
    uci set wireless.$UCI_DEV.ht_rxstream=$HT_RxStream_5g
}



mbssid_config()
{
    BssidNum2=`nvram get test3 BssidNum`
    BssidNum5=`nvram get test1 BssidNum`
    WmmCapable2=`nvram get test3 WmmCapable`
    WmmCapable5=`nvram get test1 WmmCapable`
    APSDCapable2=`nvram get test3 APSDCapable`
    APSDCapable5=`nvram get test1 APSDCapable`
    RekeyInterval2=`nvram get test3 RekeyInterval`
    RekeyInterval5=`nvram get test1 RekeyInterval`
    
    i=0
	j=0
    while [ $i -lt $BssidNum2 ]
    do    
    IFACE_IDX=$j
	IFACE_5IDX=`expr $j + 1`
    IFACE_2G_NAME=rax$i
    IFACE_5G_NAME=ra$i
	
	j=`expr $j + 2`
	i=`expr $i + 1`


    SSID2=`nvram get test3 SSID$i`
    AuthMode2=`nvram get test3 AuthMode|cut -d ";" -f $i`
    EncrypType2=`nvram get test3 EncrypType|cut -d ";" -f $i`
    WPAPSK2=`nvram get test3 WPAPSK$i`

    SSID5=`nvram get test1 SSID$i`
    AuthMode5=`nvram get test1 AuthMode|cut -d ";" -f $i`
    EncrypType5=`nvram get test1 EncrypType|cut -d ";" -f $i`
    WPAPSK5=`nvram get test1 WPAPSK$i`
 	
    IFACE_SECTION=""
    IFACE_SECTION=`uci get wireless.@wifi-iface[$IFACE_IDX]`
    if [ "$IFACE_SECTION" = "" ]; then
        uci add wireless wifi-iface
    fi
    
    uci set wireless.@wifi-iface[$IFACE_IDX]=wifi-iface
    uci set wireless.@wifi-iface[$IFACE_IDX].device=mt7615e2
    uci set wireless.@wifi-iface[$IFACE_IDX].network=lan
    uci set wireless.@wifi-iface[$IFACE_IDX].mode=ap
    uci set wireless.@wifi-iface[$IFACE_IDX].ifname=$IFACE_2G_NAME
    uci set wireless.@wifi-iface[$IFACE_IDX].wmm=$WmmCapable2
    uci set wireless.@wifi-iface[$IFACE_IDX].apsd=$APSDCapable2
    uci set wireless.@wifi-iface[$IFACE_IDX].rekeyinteval=$RekeyInterval2
    uci set wireless.@wifi-iface[$IFACE_IDX].ssid="$SSID2"
    
    if [ "$EncrypType2" == "NONE" ] && [ "$AuthMode2" == "OPEN" ]; then
        uci set wireless.@wifi-iface[$IFACE_IDX].encryption=none
    elif [ "$EncrypType2" == "AES" ] && [ "$AuthMode2" == "WPA2PSK" ]; then
        uci set wireless.@wifi-iface[$IFACE_IDX].encryption=psk2+ccmp
    elif [ "$EncrypType2" == "AES" ] && [ "$AuthMode2" == "WPAPSKWPA2PSK" ]; then
        uci set wireless.@wifi-iface[$IFACE_IDX].encryption=psk-mixed+ccmp
    elif [ "$EncrypType2" == "TKIPAES" ] && [ "$AuthMode2" == "WPA2PSK" ]; then
        uci set wireless.@wifi-iface[$IFACE_IDX].encryption=psk2+tkip+ccmp
    elif [ "$EncrypType2" == "TKIPAES" ] && [ "$AuthMode2" == "WPAPSKWPA2PSK" ]; then
        uci set wireless.@wifi-iface[$IFACE_IDX].encryption=psk-mixed+tkip+ccmp
    fi
    
    uci set wireless.@wifi-iface[$IFACE_IDX].key="$WPAPSK2"

    IFACE_SECTION=""
    IFACE_SECTION=`uci get wireless.@wifi-iface[$IFACE_5IDX]`
    if [ "$IFACE_SECTION" = "" ]; then
        uci add wireless wifi-iface
    fi
    
    uci set wireless.@wifi-iface[$IFACE_5IDX]=wifi-iface
    uci set wireless.@wifi-iface[$IFACE_5IDX].device=mt7615e5
    uci set wireless.@wifi-iface[$IFACE_5IDX].network=lan
    uci set wireless.@wifi-iface[$IFACE_5IDX].mode=ap
    uci set wireless.@wifi-iface[$IFACE_5IDX].ifname=$IFACE_5G_NAME
    uci set wireless.@wifi-iface[$IFACE_5IDX].wmm=$WmmCapable5
    uci set wireless.@wifi-iface[$IFACE_5IDX].apsd=$APSDCapable5
    uci set wireless.@wifi-iface[$IFACE_5IDX].rekeyinteval=$RekeyInterval5
    uci set wireless.@wifi-iface[$IFACE_5IDX].ssid="$SSID5"
    
    if [ "$EncrypType5" == "NONE" ] && [ "$AuthMode5" == "OPEN" ]; then
        uci set wireless.@wifi-iface[$IFACE_5IDX].encryption=none
    elif [ "$EncrypType5" == "AES" ] && [ "$AuthMode5" == "WPA2PSK" ]; then
        uci set wireless.@wifi-iface[$IFACE_5IDX].encryption=psk2+ccmp
    elif [ "$EncrypType5" == "AES" ] && [ "$AuthMode5" == "WPAPSKWPA2PSK" ]; then
        uci set wireless.@wifi-iface[$IFACE_5IDX].encryption=psk-mixed+ccmp
    elif [ "$EncrypType5" == "TKIPAES" ] && [ "$AuthMode5" == "WPA2PSK" ]; then
        uci set wireless.@wifi-iface[$IFACE_5IDX].encryption=psk2+tkip+ccmp
    elif [ "$EncrypType5" == "TKIPAES" ] && [ "$AuthMode5" == "WPAPSKWPA2PSK" ]; then
        uci set wireless.@wifi-iface[$IFACE_5IDX].encryption=psk-mixed+tkip+ccmp
    fi
    
    uci set wireless.@wifi-iface[$IFACE_5IDX].key="$WPAPSK5"

    
    done
    
    IFACE_COUNT=$i
    #echo $IFACE_COUNT
}


UCI_DEV="mt7615e5"
NVRAM_SECTION="test1"

basic_config
advanced_config
ht_config

UCI_DEV="mt7615e2"
NVRAM_SECTION="test3"

basic_config
advanced_config
ht_config

IFACE_COUNT=0
IFACE_IDX=0

mbssid_config

uci commit wireless
