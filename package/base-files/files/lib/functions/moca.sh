#!/bin/sh
# Copyright (C) 2018 Zinwell

MOCACFG="/etc/config/moca"

initconfig() {
    [ -e ${MOCACFG} ] && return
    touch ${MOCACFG}
    echo "config moca setting" > ${MOCACFG}
    uci set moca.setting.networksearch=1
    uci set moca.setting.lof=1150
    uci set moca.setting.beacontxpower=10 
    uci set moca.setting.securitymodebandd=0 
    uci set moca.setting.mocapasswordbandd=99999999988888888 
    uci set moca.setting.preferrednc=0
    uci commit moca
}

reloadmoca() {
    PARAMETER=networksearch
    VALUE=`uci get moca.setting.networksearch`
    clnkcfg -s --${PARAMETER}=${VALUE}
    
    PARAMETER=lof
    VALUE=`uci get moca.setting.lof`
    clnkcfg -s --${PARAMETER}=${VALUE}
    
    PARAMETER=beacontxpower
    VALUE=`uci get moca.setting.beacontxpower`
    clnkcfg -s --${PARAMETER}=${VALUE}
    
    PARAMETER=securitymodebandd
    VALUE=`uci get moca.setting.securitymodebandd`
    clnkcfg -s --${PARAMETER}=${VALUE}
    
    PARAMETER=mocapasswordbandd
    VALUE=`uci get moca.setting.mocapasswordbandd`
    clnkcfg -s --${PARAMETER}=${VALUE}
    
    PARAMETER=preferrednc
    VALUE=`uci get moca.setting.preferrednc`
    clnkcfg -s --${PARAMETER}=${VALUE}
    
    clnkrst
}

case ${1} in
        "init")               
                initconfig           
                ;;
         "reload")						
                reloadmoca
                ;;
        
esac
                