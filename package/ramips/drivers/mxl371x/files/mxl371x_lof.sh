#!/bin/sh

org_link_status=0

get_link_status()
{
	prev_link_status=$link_status
	clnkstat | grep "LINK UP" >> /dev/null
	if [ $? -eq 0 ]; then
	link_status=1
	else
	link_status=0
	fi
}

get_beacon_channel()
{
	beacon_channel=`clnkstat -d | grep "Beacon Channel:"| cut -c 25-28`
}

get_dhcp_ip()
{
	autoip=`uci get network.lan.proto`
	if [ "$autoip" = "dhcp" ]; then
		killall -SIGUSR2 udhcpc
    sleep 1
		killall -SIGUSR1 udhcpc
	fi
}

update_lof()
{
	get_link_status
	if [ $org_link_status != $link_status ]; then
		org_link_status=$link_status
		if [ $link_status = 1 ]; then
			get_dhcp_ip
			get_beacon_channel
			moca_lof=`uci get moca.setting.lof`
			if [ $moca_lof != $beacon_channel ]; then
				uci set moca.setting.lof=$beacon_channel
        uci commit moca
				echo "update moca_lof to $beacon_channel"
			fi
      exit 0
		fi
	fi
}

sleep 3;

while [ true ]
do
		sleep 10;
		update_lof
done
