#!/bin/sh

#. /sbin/config.sh  // hsiwei
#. /sbin/global.sh  // hsiwei
. /lib/functions/system.sh

fname="/etc/clink/clink.backup"
fbak="/etc/clink/clink.backup_bak"

applySettings(){

	moca_scan=`uci get moca.setting.networksearch`
	moca_lof=`uci get moca.setting.lof`
	moca_security=`uci get moca.setting.securitymodebandd`
	moca_passwd=`uci get moca.setting.mocapasswordbandd`
	moca_preferrednc=`uci get moca.setting.preferrednc`
	moca_power=`uci get moca.setting.beacontxpower`

	if [ "$moca_scan" != "" ]; then
		sed -e '/networksearch/d' $fname > $fbak
  	echo "networksearch 1 $moca_scan" >> $fbak
  	cat $fbak > $fname
  	rm -f $fbak
  fi
  
  if [ "$moca_lof" != "" ]; then
  	sed -e '/lof/d' $fname > $fbak
  	echo "lof 4 $moca_lof" >> $fbak
  	cat $fbak > $fname
  	rm -f $fbak
  fi

	if [ "$moca_security" != "" ]; then
		sed -e '/securitymodebandd/d' $fname > $fbak
  	echo "securitymodebandd 1 $moca_security" >> $fbak
  	cat $fbak > $fname
  	rm -f $fbak
  fi

	if [ "$moca_passwd" != "" ]; then
		length=`expr length "$moca_passwd"`
		sed -e '/mocapasswordbandd /d' $fname > $fbak
  	echo "mocapasswordbandd $length $moca_passwd" >> $fbak
  	cat $fbak > $fname
  	rm -f $fbak
  fi

	if [ "$moca_preferrednc" != "" ]; then
		sed -e '/preferrednc/d' $fname > $fbak
  	echo "preferrednc 1 $moca_preferrednc" >> $fbak
  	cat $fbak > $fname
  	rm -f $fbak
  fi
  
  if [ "$moca_power" != "" ]; then
		sed -e '/beacontxpower/d' $fname > $fbak
  	echo "beacontxpower 1 $moca_power" >> $fbak
  	cat $fbak > $fname
  	rm -f $fbak
  fi
  
}

start_370x_rgmii(){

moca_mac=`eth_mac r moca`

ifconfig eth3 up
brctl addif br0 eth3
ifconfig eth3 hw ether $moca_mac

cp -ar /etc_ro/clink /etc/clink
cp /etc_ro/platform.conf  /etc
cp /etc_ro/platform.conf  /etc/clink

applySettings

insmod /lib/modules/3.10.14\+/kernel/arch/mips/mxl370x_rgmii_v221/mxl_moca_ctrl.ko
clnkrst --macaddr $moca_mac --kclinkd
#sleep 1
#clnkmem -w -o 0x8100094 -v 0x60000
#echo 0 > /proc/moca_reset
#sleep 3
#/sbin/mxl37xx_reset.sh &
/sbin/mxl37xx_lof.sh &

}

start_370x_pcie(){

moca_mac=`eth_mac r moca`

cp -ar /etc_ro/clink /etc/clink
cp /etc_ro/platform.conf  /etc
cp /etc_ro/platform.conf  /etc/clink

applySettings

#insmod /lib/modules/3.10.14\+/kernel/arch/mips/mxl370x_pcie_v291/mxl_moca_pcie.ko verbose=0 intr_sel_type=2 mac_addr="$moca_mac"
#insmod /lib/modules/3.10.14\+/kernel/arch/mips/mxl370x_pcie_v291/mxl_moca_ctrl.ko
insmod /lib/modules/3.10.14/mxl_moca_pcie.ko verbose=0 intr_sel_type=2 mac_addr="$moca_mac"
insmod /lib/modules/3.10.14/mxl_moca_ctrl.ko
ifconfig en0 up
brctl addif br0 en0
brctl setageing br0 3600
sleep 2
clnkrst --macaddr $moca_mac --kclinkd
sleep 2
/sbin/mxl37xx_lof.sh &

if [ -d "/sys/class/net/en0" ]; then
	#echo 4 > /proc/irq/25/smp_affinity #PCIe2
	#echo 7 > /sys/class/net/en0/queues/rx-0/rps_cpus
	#echo 5 > /sys/class/net/en0/queues/tx-0/xps_cpus
	echo 8 > /proc/irq/3/smp_affinity  #GMAC
	echo 2 > /proc/irq/24/smp_affinity #PCIe1
	echo 4 > /proc/irq/25/smp_affinity #PCIe2
	echo 8 > /sys/class/net/en0/queues/rx-0/rps_cpus
	echo 3 > /sys/class/net/ra0/queues/rx-0/rps_cpus
	echo 3 > /sys/class/net/rax0/queues/rx-0/rps_cpus
	echo 5 > /sys/class/net/eth2/queues/rx-0/rps_cpus
	echo 5 > /sys/class/net/eth3/queues/rx-0/rps_cpus
	echo "en0 RPS: CPU 3"
fi

}

check_moca_daemon(){
beacon_channel=`clnkstat -d | grep "Beacon Channel:"| cut -c 25-28`
if [ "$beacon_channel" = "" ]; then
	lan_mac=$(cat /sys/devices/virtual/net/eth0/address)
	moca_mac=$(macaddr_add "$lan_mac" 1)
	clnkrst --macaddr $moca_mac --kclinkd
fi
}

start_371x_pcie(){
sleep 5
#moca_mac=`eth_mac r moca`
lan_mac=$(cat /sys/devices/virtual/net/eth0/address)
moca_mac=$(macaddr_add "$lan_mac" 1)

rm -rf /etc/clink
cp -ar /etc_ro/clink /etc/clink
cp /etc_ro/platform.conf  /etc
cp /etc_ro/platform.conf  /etc/clink

applySettings

#insmod /lib/modules/3.10.14\+/kernel/arch/mips/mxl371x_pcie_v1113/mxl_moca_pcie.ko mac_addr="$moca_mac"
insmod /lib/modules/3.10.14/mxl_moca_pcie.ko mac_addr="$moca_mac"
ifconfig en0 up
brctl addif br-lan en0
brctl setageing br-lan 3600
sleep 2
#insmod /lib/modules/3.10.14\+/kernel/arch/mips/mxl371x_pcie_v1113/mxl_moca_ctrl.ko
insmod /lib/modules/3.10.14/mxl_moca_ctrl.ko
sleep 2
clnkrst --macaddr $moca_mac --kclinkd
#sleep 8
#check_moca_daemon
/sbin/mxl371x_lof.sh &

if [ -d "/sys/class/net/en0" ]; then
	#echo 4 > /proc/irq/25/smp_affinity #PCIe2
	#echo 7 > /sys/class/net/en0/queues/rx-0/rps_cpus
	##echo 5 > /sys/class/net/en0/queues/tx-0/xps_cpus
	echo 1 > /proc/irq/3/smp_affinity  #GMAC
	echo 2 > /proc/irq/24/smp_affinity #PCIe1
	echo 4 > /proc/irq/25/smp_affinity #PCIe2
	echo 8 > /sys/class/net/en0/queues/rx-0/rps_cpus
	echo 8 > /sys/class/net/ra0/queues/rx-0/rps_cpus
	echo 8 > /sys/class/net/rax0/queues/rx-0/rps_cpus
	echo 8 > /sys/class/net/eth0/queues/rx-0/rps_cpus
#	echo 8 > /sys/class/net/eth3/queues/rx-0/rps_cpus
	echo "en0 RPS: CPU 3"
fi

}

case "$1" in
    370x_rgmii)
        start_370x_rgmii
        ;;
    370x_pcie)
        start_370x_pcie
        ;;   
    371x_pcie)
        start_371x_pcie
        ;;    
esac

exit 0


