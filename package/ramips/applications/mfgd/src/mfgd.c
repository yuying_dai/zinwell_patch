/*
 *      Operation routines for Command Line Interface access
 *
 *      Authors: Slack
 *
 *      
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <errno.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <net/route.h>
#include <linux/atm.h>
#include <linux/atmdev.h>
#include <crypt.h>

#define MFGD_VERSION		"v1.3a"
//#define DAEMON_PORT 9801 
unsigned short int DAEMON_PORT;

int DaemonSocket;  
struct sockaddr_in DaemonAddr;  
int ClientSocket=0;  
struct sockaddr_in HostAddr;

int set_led(int port, int boolOn);
unsigned long get_button(int port);

void MListen(void);
//void RunCmd(char *CmdString);
int BindSocket(void);
int incoming_call(void); 
int ConnectClient(void);  
int exec_cmd(char *cmd,char *buf,int len);

#define ZW_VERSION "OPENWRT"
#define ZW_MODEL "ZMW-D398L"
#define CUST_ID "VZ"

int hexcharToint(char *s){
	char a[1];
	strncpy(a, s, 1);
	int v;
	if(*a>='A'-1){
		printf("string: %s=%d %s-9=%d\n", a, *a, a, *a-('A'));
		v = 10+(*a-('A'));
		return v;
	}
	return  atoi(a);	
}

void doiPATCSetting2(void){
	char buf[12]="\0";
	char b[7]="\0";
	unsigned int val;
	unsigned char cmd[128];
	
	FILE *fd = popen("iwpriv ra0 e2p 55|grep 55|cut -d \":\" -f2", "r");
	
	fgets(buf, 12, fd);
	val = ((hexcharToint(buf+4)*16)+hexcharToint(buf+5))&0x7f;
	pclose(fd);
	
	fd = popen("iwpriv ra0 e2p 36|grep 36|cut -d \":\" -f2", "r"); // check iPATC enable
	fgets(b, 7, fd);
	
	//if(!strcmp(b, "0x6000")){
		system("iwpriv ra0 e2p 36=6002");
		
		//2.4G
		sprintf(cmd, "iwpriv ra0 e2p 197=%x", val);
		system(cmd);
		system("iwpriv ra0 e2p 198=00");
		sprintf(cmd, "iwpriv ra0 e2p 199=%x", val);
		system(cmd);
		sprintf(cmd,"iwpriv ra0 e2p 19a=%x",val+28);
		system(cmd);
		system("iwpriv ra0 e2p 19b=ff");
		system("iwpriv ra0 e2p 19c=ff");
		system("iwpriv ra0 e2p 19d=ff");
		system("iwpriv ra0 e2p 19e=ff");
		system("iwpriv ra0 e2p 19f=ff");
		system("iwpriv ra0 e2p 1a0=ff");
		
		sprintf(cmd, "iwpriv ra0 e2p 1a9=%x", val);
		system(cmd);
		system("iwpriv ra0 e2p 1aa=00");
		sprintf(cmd, "iwpriv ra0 e2p 1ab=%x", val);
		system(cmd);
		sprintf(cmd,"iwpriv ra0 e2p 1ac=%x",val+12);
		system(cmd);
		sprintf(cmd,"iwpriv ra0 e2p 1ad=%x",val+14);
		system(cmd);
		sprintf(cmd,"iwpriv ra0 e2p 1ae=%x",val+16);
		system(cmd);
		sprintf(cmd,"iwpriv ra0 e2p 1af=%x",val+18);
		system(cmd);
		system("iwpriv ra0 e2p 1b0=ff");
		system("iwpriv ra0 e2p 1b1=ff");
		system("iwpriv ra0 e2p 1b2=ff");
		
		system("iwpriv ra0 set bufferWriteBack=2");
	//}
	pclose(fd);
}

//////////////////////////////////////////////////////////////////////
int main(int argc, char** argv)
{
    

    FILE *fp;
    char line[20]="";
    pid_t pid;	
    char buffer[9]="";
    
    printf("\n MFG daemon %s \n",MFGD_VERSION);			
    
    //system("telnetd");
    //printf("Enter Factory Mode and Enable telnetd\n");

    if ((fp = fopen("/var/run/mfgd.pid", "r")) != NULL) {
        fgets(line, sizeof(line), fp);
        if ( sscanf(line, "%d", &pid) ) {
            if (pid > 1)
                kill(pid, SIGTERM);
        }
        fclose(fp);
    }
    
    if ((fp = fopen("/var/run/mfgd.pid", "w")) == NULL) {
        printf("Can't create PID file!");
        return -1;
    }

    fprintf(fp,"%d\n",getpid());
    fclose(fp);
	
	
    pid_t child_pid1;

    if(argc > 1){
        DAEMON_PORT = atoi(argv[1]);
    }else{
        DAEMON_PORT = 9801;
    }

    if((child_pid1 = fork()) < 0 )
        _exit(1); 
    if(child_pid1 == 0) {  
        if (!BindSocket()) {  
            printf("Can not bind socket!\n");
            _exit(1);  
        }
        MListen();   
    }
    else
        wait(NULL); 

    return 0;
}    

/////////////////////////////////// 
void MListen()
{
    time_t now; 
    char *comval;
    char comstr1[32],comstr2[4096];
    fd_set sock;  
    struct timeval tv;  
    int t;  
    char BUF[192];  
    char CC[2];  
    int n;  
    char *seps = ",";      
    FILE *fp;
    char cmd[256];
    char buffer[4096]="";
    char tmpMac[17]="";
    unsigned int uiStart1,uiStart2,uiStart3,uiStart4,uiStart5,uiStart6;
    unsigned int uiStartVal;

    now = time(NULL); 
    printf("execute in pid %d of MListen %s\n",getpid(),asctime((const struct tm*)localtime(&now)));   
    ///////////////////////////////////////////////////////////////////////////
    do {  
        if (incoming_call()) {
            if (ConnectClient()) {  
                FD_ZERO(&sock);  
                FD_SET(ClientSocket,&sock);  
                n = 0;  
                do {  
                    tv.tv_sec = 60; tv.tv_usec = 0;  
                    t = select(ClientSocket+1,&sock,NULL,NULL,&tv);  
                    if (t<=0||!FD_ISSET(ClientSocket,&sock)) ;  
                        read(ClientSocket,CC,1);  
                    if (CC[0]==13||CC[0]==10||CC[0]==0) {  
                        BUF[n] = 0;
                        printf("===>%s\n",BUF);     
                        if(strlen(BUF)>0){
                            comval = strtok( BUF, seps );
                            strcpy(comstr1,comval); 
                            //printf("===>%s\n",comstr1);
                            while( comval != NULL )
                            {
                                strcpy(comstr2,comval); 
                                //printf("===>%s\n",comstr2);
                                comval = strtok( NULL, seps );  
                            }
                        }else{
                            strcpy(comstr1,""); 
                        }
                        // modify to add your commends here....
                        if(!strcmp(comstr1,"REVOKE")){
                            //restore default						
                            sprintf(cmd,"jffs2reset -y");
                            system(cmd);
                            //sprintf(cmd,"nvram restore test1 /etc/zw_configs/D398_2860_default");
                            //system(cmd);
							//sprintf(cmd,"nvram restore test3 /etc/zw_configs/D398_wifi3_default.octet-stream");
                            //system(cmd);
                            sprintf(cmd,"sleep 1");
                            system(cmd);
                            sprintf(cmd,"sync");
                            system(cmd);
                            strcpy(buffer,"done");
                        }
                        else if(!strcmp(comstr1,"GETETHMAC")){
                            exec_cmd("eth_mac r lan",cmd,sizeof(cmd));
                            printf("result = \"%s\"\n",cmd);
                            sscanf(cmd,"%X:%X:%X:%X:%X:%X",&uiStart1,&uiStart2,&uiStart3,&uiStart4,&uiStart5,&uiStart6);
                            sprintf(buffer,"%02X%02X%02X%02X%02X%02X",uiStart1,uiStart2,uiStart3,uiStart4,uiStart5,uiStart6);
                        }
                        else if(!strcmp(comstr1,"GETMOCAMAC")){
                            exec_cmd("eth_mac r moca",cmd,sizeof(cmd));
                            printf("result = \"%s\"\n",cmd);
                            sscanf(cmd,"%X:%X:%X:%X:%X:%X",&uiStart1,&uiStart2,&uiStart3,&uiStart4,&uiStart5,&uiStart6);
                            sprintf(buffer,"%02X%02X%02X%02X%02X%02X",uiStart1,uiStart2,uiStart3,uiStart4,uiStart5,uiStart6);
                        }
                        else if(!strcmp(comstr1,"GETWIFIMAC")){
                            exec_cmd("iwpriv ra0 e2p 4|grep 0x",cmd,sizeof(cmd));
                            printf("result = \"%s\"\n",cmd);
                            sscanf(cmd,"[0x0004]:0x%02X%02X",&uiStart2,&uiStart1);
                            exec_cmd("iwpriv ra0 e2p 6|grep 0x",cmd,sizeof(cmd));
                            printf("result = \"%s\"\n",cmd);
                            sscanf(cmd,"[0x0006]:0x%02X%02X",&uiStart4,&uiStart3);
                            exec_cmd("iwpriv ra0 e2p 8|grep 0x",cmd,sizeof(cmd));
                            printf("result = \"%s\"\n",cmd);
                            sscanf(cmd,"[0x0008]:0x%02X%02X",&uiStart6,&uiStart5);
                            sprintf(buffer,"%02X%02X%02X%02X%02X%02X",uiStart1,uiStart2,uiStart3,uiStart4,uiStart5,uiStart6);
                        }
                        else if(!strcmp(comstr1,"SETMAC")){
                            sprintf(tmpMac,"%c%c:%c%c:%c%c:%c%c:%c%c:%c%c",comstr2[0],comstr2[1],comstr2[2],comstr2[3],comstr2[4],comstr2[5],comstr2[6],comstr2[7],comstr2[8],comstr2[9],comstr2[10],comstr2[11]);
                            sscanf(tmpMac,"%X:%X:%X:%X:%X:%X",&uiStart1,&uiStart2,&uiStart3,&uiStart4,&uiStart5,&uiStart6);
                            sprintf(cmd,"eth_mac w lan %02x %02x %02x %02x %02x %02x",uiStart1,uiStart2,uiStart3,uiStart4,uiStart5,uiStart6);
                            printf("exec:\"%s\"\n",cmd);
                            system(cmd);
							
							//WiFi MAC = LAN MAC + 2
                            uiStartVal=(uiStart4<<16)+(uiStart5<<8)+uiStart6+2;
                            //iwpriv ra0 e2p 4=0500
                            //iwpriv ra0 e2p 6=989e
                            //iwpriv ra0 e2p 8=0494
                            sprintf(cmd,"iwpriv ra0 e2p 4=%02x%02x",uiStart2,uiStart1);
                            printf("exec:\"%s\"\n",cmd);
                            system(cmd);
                            sprintf(cmd,"iwpriv ra0 e2p 6=%02x%02x",(uiStartVal&0xff0000)>>16,uiStart3);
                            printf("exec:\"%s\"\n",cmd);
                            system(cmd);
                            sprintf(cmd,"iwpriv ra0 e2p 8=%02x%02x",uiStartVal&0xff,(uiStartVal&0xff00)>>8);
                            printf("exec:\"%s\"\n",cmd);
                            system(cmd); 
                            sprintf(cmd,"iwpriv ra0 set bufferWriteBack=2");
                            printf("exec:\"%s\"\n",cmd);
                            system(cmd);
                            strcpy(buffer,"done");
                        }
                        else if(!strcmp(comstr1,"GETSN")){
                            strcpy(buffer,"done");
                        }
                        else if(!strcmp(comstr1,"SETSN")){
                            strcpy(buffer,"done");
                        }
                        else if(!strcmp(comstr1,"REBOOT")){
                            system("reboot -f");
                            strcpy(buffer,"done");
                        }
                        else if(!strcmp(comstr1,"GETFWVER")){
                            //sprintf(buffer,"%s",ZW_VERSION);
							exec_cmd("cat /etc/zversion",buffer,sizeof(buffer));
                        }
                        else if(!strcmp(comstr1,"GETMODEL")){
                            sprintf(buffer,"%s",ZW_MODEL);
                        }
                        else if(!strcmp(comstr1,"GETCUSTOMER")){
                            //returns device��s customer ID
                            sprintf(buffer,"%s",CUST_ID);
                        }
                        else if(!strcmp(comstr1,"GETHWVER")){
                        
                        }
                        else if(!strcmp(comstr1,"SETHWVER")){
                            strcpy(buffer,"done");
                        }
                        else if(!strcmp(comstr1,"GETCOUNTRY")){
                        
                        }
                        else if(!strcmp(comstr1,"SETCOUNTRY")){
                            strcpy(buffer,"done");              	
                        }
                        else if(!strcmp(comstr1,"ISBTNPRESS")){
                        
                        }
                        else if(!strcmp(comstr1,"GETRESETBTN")){
                        
                        }
                        else if(!strcmp(comstr1,"GETWPSBTN")){
                        
                        }
                        else if(!strcmp(comstr1,"GETCHBW")){
                        
                        }
                        else if(!strcmp(comstr1,"SETTCEN")){
                        		doiPATCSetting2();
                        		strcpy(buffer,"done");
                        }
                        else if(!strcmp(comstr1,"SETBW")){
                        
                        }
                        else if(!strcmp(comstr1,"SET5GCH")){
                        		sprintf(cmd,"iwpriv ra0 set Channel=%s",comstr2);
                            system(cmd);
                            printf("exec:\"%s\"\n",cmd);
                        }
                        else if(!strcmp(comstr1,"SET5GSSID")){
                        		sprintf(cmd,"iwpriv ra0 set SSID=%s",comstr2);
                            system(cmd);
                            printf("exec:\"%s\"\n",cmd);
                        }
                        else if(!strcmp(comstr1,"SET2GCH")){
                        		sprintf(cmd,"iwpriv rax0 set Channel=%s",comstr2);
                            system(cmd);
                            printf("exec:\"%s\"\n",cmd);
                        }
                        else if(!strcmp(comstr1,"SET2GSSID")){
                        		sprintf(cmd,"iwpriv rax0 set SSID=%s",comstr2);
                            system(cmd);
                            printf("exec:\"%s\"\n",cmd);
                        }
                        else if(!strcmp(comstr1,"EXEC")){
                            exec_cmd(comstr2,buffer,sizeof(buffer));
                        }
                        
                        ///////response to client/////////////////////////////  
                        if(strlen(buffer)>0)
                            send(ClientSocket, buffer, strlen(buffer), 0);   
                        else
                            send(ClientSocket, "-1", 2, 0);   
         	  
                        close(ClientSocket);  
                        break;  
                        bzero(BUF, 128); 
                        n = 0;  
                    } else   
                        BUF[n]=CC[0]; n++;                         
                } while (1);  
            }  
        }  
    } while (1);
}

////////////////////////////////////////////////////////////////
int BindSocket(void)  
{  
    time_t now; 
    
    now = time(NULL); 
    printf("execute in pid %d of BindSocket %s\n",getpid(),asctime((const struct tm*)localtime(&now)));   
  
    DaemonSocket = socket(AF_INET,SOCK_STREAM,0);  
    if (DaemonSocket==-1)
        return 0;  
    DaemonAddr.sin_family = AF_INET;  
    DaemonAddr.sin_port   = htons(DAEMON_PORT);  
    if (bind(DaemonSocket,(struct sockaddr*)&DaemonAddr,sizeof(DaemonAddr))<0) {  
        printf("Can not bind!\n");  
        return 0;  
    }  
    if (listen(DaemonSocket,1024)!=0) {  
        printf("Can not listen!\n");  
        return 0;  
    }  
    
    return 1;  
}
///////////////////////////////////////////
int incoming_call(void)  
{  
    fd_set sock;  
    struct timeval tv;  
    int t;  

    FD_ZERO(&sock);  
    FD_SET(DaemonSocket,&sock);  
    tv.tv_sec = 60; tv.tv_usec = 0;  
    t = select(DaemonSocket + 1,&sock,NULL,NULL,&tv);  
    if (t<=0||!FD_ISSET(DaemonSocket,&sock)) return 0;  

    return 1;  
}  
////////////////////////////////////////////////////////////
int ConnectClient(void)  
{  
    int socksize=sizeof(HostAddr);  
    unsigned char * addr;  
    
    ClientSocket = accept(DaemonSocket,(struct sockaddr*)&HostAddr,&socksize);  
    if (ClientSocket<0) return 0;  
    
    addr = (unsigned char *)&HostAddr.sin_addr.s_addr;  
    
    printf("\nincoming address:%d.%d.%d.%d:",addr[0],addr[1],addr[2],addr[3]);  
    
    return 1;  
}  

int exec_cmd(char *cmd,char *buf,int len)
{

    FILE *fp;
    char tmpbuf[256]="";
    
    memset(buf,'\0',sizeof(buf));
    
    //printf("%s\n",cmd);
    fp=popen(cmd,"r");
    //fgets(buffer,sizeof(buffer),fp);
    if(fp != NULL){
        while(fgets(tmpbuf,sizeof(tmpbuf),fp) != NULL){
            if(strlen(buf)+strlen(tmpbuf) >= len)
                break;
            sprintf(buf,"%s%s",buf,tmpbuf);
        }
        pclose(fp);
        return 1;
    }else{
        return 0;
    }

}
