#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>


typedef struct moca_node_info {

	unsigned short  nodeId;
	char macaddr[18];
	char mocaVer[4];
	int txRate1;
	int rxRate1;
	int txRate2;
	int rxRate2;
	int txRate3;
	int rxRate3;
	int txRate4;
	int rxRate4;
	int txRate5;
	int rxRate5;
	int rxPWR;
	int rxFrame;
	int rxFrameErr;

} MOCA_NODE_INFO_T, *MOCA_NODE_INFO_Tp;


//void getMocaPhyRate()

int GetMoCAStatus(void)
{
	FILE *fp;
	char buffer[16]="";	
	
	fp=fopen("/proc/moca_init","r");
	
	if(fp!=NULL)
	{
		fgets(buffer,sizeof(buffer),fp);
		if(strncmp(buffer, "0", 1)!=0)
		{
			fclose(fp);
			return 0;
		}
		fclose(fp);
	}else{
		//fp==NULL means system is booting...
		return 0;
	}

	return 1;
}

void main()
{
	FILE *fp;
	MOCA_NODE_INFO_T **nodeInfo;
	int count=0;
	int ncID=0;
	int myID=0;
	int i=0;
	float per=0;
	char myMacaddr[18]="";
	unsigned int uiStart1,uiStart2,uiStart3,uiStart4,uiStart5,uiStart6;
	char buf[128]="";
	int j=0;
	char *pStr = NULL;
	char *tStr = NULL;
	char m_buf[128];
	char t_buf[128];
	char tmp[20];
	int id=0;
	int index=0;
	char cmd[256]="";
	
	if(!GetMoCAStatus())
	{
		return;
	}
	
	nodeInfo=(MOCA_NODE_INFO_T **)malloc(16*sizeof(MOCA_NODE_INFO_T *));
	for(i=0;i<16;i++){
		nodeInfo[i]=(MOCA_NODE_INFO_T *)malloc(1*sizeof(MOCA_NODE_INFO_T));
		memset(nodeInfo[i]->macaddr,'\0',sizeof(nodeInfo[i]->macaddr));
		memset(nodeInfo[i]->mocaVer,'\0',sizeof(nodeInfo[i]->mocaVer));
		nodeInfo[i]->txRate1=0;
		nodeInfo[i]->rxRate1=0;
		nodeInfo[i]->txRate2=0;
		nodeInfo[i]->rxRate2=0;
		nodeInfo[i]->txRate3=0;
		nodeInfo[i]->rxRate3=0;
		nodeInfo[i]->txRate4=0;
		nodeInfo[i]->rxRate4=0;
		nodeInfo[i]->txRate5=0;
		nodeInfo[i]->rxRate5=0;
		nodeInfo[i]->rxPWR=0;
		nodeInfo[i]->rxFrame=0;
		nodeInfo[i]->rxFrameErr=0;
	}
	
	fp = popen("clnkstat --netinfo","r");
	//sleep(2);
	if(fp == NULL) return;	
	while(fgets(buf,sizeof(buf),fp)!=NULL){
		// printf("%s",buf);
		if(!strncmp(buf,"Info for node",13)){
			//15-16
			//memset(tmp,'\0',sizeof(tmp));
			//strncpy(tmp,buf+14,20);
			//pStr=strchr(tmp,':');
			//if(pStr)
			// *(pStr) = '\0';
			//pStr = NULL;
			sscanf(buf, "Info for node %d:",&id);
			nodeInfo[index]->nodeId=id;
			//nodeIdList[index]=nodeInfo[index].nodeId;
			index++;
			//printf("index=%d\n",index);
			count++;
		}
		else if(!strncmp(buf,"Node State",10)){
			//62~78
			//nodeInfo[index].macaddr
			//memset(tmp,'\0',sizeof(tmp));
			if(index > 0){

				strncpy(nodeInfo[index-1]->macaddr,buf+61,17);
				
			}
			
			
			
		}
		else if(!strncmp(buf,"Node Moca Version",17)){
			//30~32
			//nodeInfo[index].mocaVer
			if(index > 0)
			{
			    pStr=strchr(buf,'.');
				if(pStr)
				{
				  snprintf(nodeInfo[index-1]->mocaVer, 4, "%s", pStr - 1);
				  pStr = NULL;
				  //printf("nodeInfo[index-1]->mocaVer=%s\n",nodeInfo[index-1]->mocaVer);
				}
			}			
		}
		else if((tStr=strstr(buf,"RX Frames:"))!=0)
		{
			//51-68
			memset(m_buf,0,sizeof(m_buf));
            snprintf(m_buf, 128, "%s", tStr + 1 + strlen("RX Frames:"));
		    tStr = NULL;
			if(index > 0)
			{
				nodeInfo[index-1]->rxFrame=atoi(m_buf);
				//printf("nodeInfo[index-1]->rxFrame=%d\n",nodeInfo[index-1]->rxFrame);
			}
		}
		else if((tStr=strstr(buf,"RX Frame Errors:"))!=NULL)
		{
		    memset(m_buf,0,sizeof(m_buf));
            snprintf(m_buf, 128, "%s", tStr + 1 + strlen("RX Frame Errors:"));
		    tStr = NULL;
			if(index > 0)
			{
				nodeInfo[index-1]->rxFrameErr=atoi(m_buf);
				//printf("nodeInfo[index-1]->rxFrameErr=%d\n",nodeInfo[index-1]->rxFrameErr);
			}
		}
	}
	pclose(fp);
	
	fp = popen("cat /sys/devices/virtual/net/en0/address","r");
	//sleep(2);
	if(fp == NULL) return;	
	while(fgets(buf,sizeof(buf),fp)!=NULL){
		sscanf(buf,"%X:%X:%X:%X:%X:%X",&uiStart1,&uiStart2,&uiStart3,&uiStart4,&uiStart5,&uiStart6);
        sprintf(myMacaddr,"%02x:%02x:%02x:%02x:%02x:%02x",uiStart1,uiStart2,uiStart3,uiStart4,uiStart5,uiStart6);
		//printf("myMacaddr=%s\n",myMacaddr);
	}
	pclose(fp);
	
	
	for(j=0;j<count;j++)
	{
	    if(!strncmp(nodeInfo[j]->mocaVer,"1.1",3))
	      sprintf(cmd,"clnkstat --phyinfo=ucast,%d",nodeInfo[j]->nodeId);
		else
		  sprintf(cmd,"clnkstat --phyinfo=bcast,%d",nodeInfo[j]->nodeId);

		if(strncmp(nodeInfo[j]->macaddr,myMacaddr,17)!=0)
		{
			//printf("%s\n",cmd);
			//sleep(1);
			index=0;
			fp = popen(cmd,"r");
			//sleep(3);
			if(fp == NULL) break;
			while(fgets(buf,sizeof(buf),fp)!=NULL)
			{
				//printf("%s",buf);
				if ((tStr = strstr(buf, "Info for node")) != NULL)
				{	
					//18~19
					memset(tmp,0,sizeof(tmp));
					snprintf(tmp, 20, "%s", tStr + 1 + strlen("Info for node"));
			        pStr=strchr(tmp,':');
			        if(pStr)
			           *(pStr) = '\0';
					tStr = NULL;
					if(nodeInfo[j]->nodeId != atoi(tmp))
					{
						//printf("node id error\n");
						return;		
					}
					//index++;
					//index=atoi(tmp);
					//printf("index=%d\n",index);
				}
				else if((tStr = strstr(buf, "TX ch0 Ucast PHY Rate:")) != NULL){
					pStr = NULL;
					memset(m_buf,0,sizeof(m_buf));
					memset(t_buf,0,sizeof(t_buf));
					sprintf(m_buf,"%s",tStr + 1 + strlen("TX ch0 Ucast PHY Rate:"));

					pStr  = strstr(m_buf, "RX ch0 Ucast PHY Rate:");
					if(pStr)
					{
					sprintf(t_buf,"%s",pStr + 1 + strlen("RX ch0 Ucast PHY Rate:"));
					nodeInfo[j]->rxRate1=atoi(t_buf);
					pStr = '\0';
					pStr = NULL; 
					nodeInfo[j]->txRate1=atoi(m_buf);
					}
					tStr = NULL;
					//printf("nodeInfo[j]->rxRate1=%d\n",nodeInfo[j]->rxRate1);
					//printf("nodeInfo[j]->txRate1=%d\n",nodeInfo[j]->txRate1);
				}
				else if((tStr = strstr(buf, "TX ch1 Ucast PHY Rate:")) != NULL){
					pStr = NULL;
					memset(m_buf,0,sizeof(m_buf));
					memset(t_buf,0,sizeof(t_buf));
					sprintf(m_buf,"%s",tStr + 1 + strlen("TX ch1 Ucast PHY Rate:"));

					pStr  = strstr(m_buf, "RX ch1 Ucast PHY Rate:");
					if(pStr)
					{
					sprintf(t_buf,"%s",pStr + 1 + strlen("RX ch1 Ucast PHY Rate:"));
					nodeInfo[j]->rxRate2=atoi(t_buf);
					pStr = '\0';
					pStr = NULL; 
					nodeInfo[j]->txRate2=atoi(m_buf);
					}
					tStr = NULL;
					//printf("nodeInfo[j]->rxRate2=%d\n",nodeInfo[j]->rxRate2);
					//printf("nodeInfo[j]->txRate2=%d\n",nodeInfo[j]->txRate2);
				}
				else if((tStr = strstr(buf, "TX ch2 Ucast PHY Rate:")) != NULL){
					pStr = NULL;
					memset(m_buf,0,sizeof(m_buf));
					memset(t_buf,0,sizeof(t_buf));
					sprintf(m_buf,"%s",tStr + 1 + strlen("TX ch2 Ucast PHY Rate:"));

					pStr  = strstr(m_buf, "RX ch2 Ucast PHY Rate:");
					if(pStr)
					{
					sprintf(t_buf,"%s",pStr + 1 + strlen("RX ch2 Ucast PHY Rate:"));
					nodeInfo[j]->rxRate3=atoi(t_buf);
					pStr = '\0';
					pStr = NULL; 
					nodeInfo[j]->txRate3=atoi(m_buf);
					}
					tStr = NULL;
					//printf("nodeInfo[j]->rxRate3=%d\n",nodeInfo[j]->rxRate3);
					//printf("nodeInfo[j]->txRate3=%d\n",nodeInfo[j]->txRate3);
				}
				else if((tStr = strstr(buf, "TX ch3 Ucast PHY Rate:")) != NULL){
					pStr = NULL;
					memset(m_buf,0,sizeof(m_buf));
					memset(t_buf,0,sizeof(t_buf));
					sprintf(m_buf,"%s",tStr + 1 + strlen("TX ch3 Ucast PHY Rate:"));

					pStr  = strstr(m_buf, "RX ch3 Ucast PHY Rate:");
					if(pStr)
					{
					sprintf(t_buf,"%s",pStr + 1 + strlen("RX ch3 Ucast PHY Rate:"));
					nodeInfo[j]->rxRate4=atoi(t_buf);
					pStr = '\0';
					pStr = NULL; 
					nodeInfo[j]->txRate4=atoi(m_buf);
					}
					tStr = NULL;
					//printf("nodeInfo[j]->rxRate4=%d\n",nodeInfo[j]->rxRate4);
					//printf("nodeInfo[j]->txRate4=%d\n",nodeInfo[j]->txRate4);
				}
				else if((tStr = strstr(buf, "TX ch4 Ucast PHY Rate:")) != NULL){
					pStr = NULL;
					memset(m_buf,0,sizeof(m_buf));
					memset(t_buf,0,sizeof(t_buf));
					sprintf(m_buf,"%s",tStr + 1 + strlen("TX ch4 Ucast PHY Rate:"));

					pStr  = strstr(m_buf, "RX ch4 Ucast PHY Rate:");
					if(pStr)
					{
					sprintf(t_buf,"%s",pStr + 1 + strlen("RX ch4 Ucast PHY Rate:"));
					nodeInfo[j]->rxRate5=atoi(t_buf);
					pStr = '\0';
					pStr = NULL; 
					nodeInfo[j]->txRate5=atoi(m_buf);
					}
					tStr = NULL;
					//printf("nodeInfo[j]->rxRate4=%d\n",nodeInfo[j]->rxRate5);
					//printf("nodeInfo[j]->txRate4=%d\n",nodeInfo[j]->txRate5);
				}
				else if((tStr = strstr(buf, "TX Ucast NP  PHY Rate:")) != NULL){
					if(!strncmp(nodeInfo[j]->mocaVer,"2.0",3))
					{
						//tx 24~32, nodeInfo[index].txRate1
						//rx 58~66, nodeInfo[index].rxRate1

						pStr = NULL;
						memset(m_buf,0,sizeof(m_buf));
						memset(t_buf,0,sizeof(t_buf));
						sprintf(m_buf,"%s",tStr + 1 + strlen("TX Ucast NP  PHY Rate:"));
						pStr  = strstr(m_buf, "RX Ucast NP  PHY Rate:");
						if(pStr)
						{
							sprintf(t_buf,"%s",pStr + 1 + strlen("RX Ucast NP  PHY Rate:"));
							if(index==0)
								nodeInfo[j]->rxRate1=atoi(t_buf);
							else if(index==1)
								nodeInfo[j]->rxRate2=atoi(t_buf);
							else if(index==2)
								nodeInfo[j]->rxRate3=atoi(t_buf);
							else if(index==3)
								nodeInfo[j]->rxRate4=atoi(t_buf);
							else if(index==4)
								nodeInfo[j]->rxRate5=atoi(t_buf);
							pStr = '\0';
							pStr = NULL; 
							if(index==0)
								nodeInfo[j]->txRate1=atoi(m_buf);
							else if(index==1)
								nodeInfo[j]->txRate2=atoi(m_buf);
							else if(index==2)
								nodeInfo[j]->txRate3=atoi(m_buf);
							else if(index==3)
								nodeInfo[j]->txRate4=atoi(m_buf);
							else if(index==4)
								nodeInfo[j]->txRate5=atoi(m_buf);
						}
						tStr = NULL;
						index++;
						//printf("nodeInfo[j]->rxRate1=%d\n",nodeInfo[j]->rxRate1);
						//printf("nodeInfo[j]->txRate1=%d\n",nodeInfo[j]->txRate1);
						//printf("nodeInfo[j]->txRate2=%d\n",nodeInfo[j]->txRate2);
						//printf("nodeInfo[j]->rxRate2=%d\n",nodeInfo[j]->rxRate2);
					}
				}
				else if ((tStr = strstr(buf, "RX Power Level:")) != NULL)
				{	
					memset(m_buf,0,sizeof(m_buf));
					sprintf(m_buf,"%s",tStr + 1 + strlen("RX Power Level:"));
					tStr = NULL;
					nodeInfo[j]->rxPWR=atoi(m_buf);
					//printf("nodeInfo[j]->rxPWR=%d\n",nodeInfo[j]->rxPWR);
					break;
				}
			}
			pclose(fp);

			sprintf(cmd,"clnkstat --phyinfo=m2map,%d",nodeInfo[j]->nodeId);
			fp = popen(cmd,"r");
			if(fp == NULL) break;
			while(fgets(buf,sizeof(buf),fp)!=NULL)
			{
				if ((tStr = strstr(buf, "RX Power Level:")) != NULL)
				{
					memset(m_buf,0,sizeof(m_buf));
					sprintf(m_buf,"%s",tStr + 1 + strlen("RX Power Level:"));
					tStr = NULL;
					nodeInfo[j]->rxPWR=atoi(m_buf);
					//printf("nodeInfo[j]->rxPWR=%d\n",nodeInfo[j]->rxPWR);
					break;
				}
			}
			pclose(fp);

		}else{
			myID=nodeInfo[j]->nodeId;
		}
	
	}
	
	fp = popen("clnkpm","r");
	if(fp == NULL) return;
	while(fgets(buf,sizeof(buf),fp)!=NULL){
		//printf("%s",buf);
		if(!strncmp(buf,"NodeID:",7)){
			//9-10 my ID
			//22-23 NC ID
			//58-59 backup NC ID
			//memset(tmp,'\0',sizeof(tmp));
			//strncpy(tmp,buf+8,2);
			//if(tmp[1] == ' ') tmp[1]='\0';
			//myID=atoi(tmp);
			//printf("myID=%d\n",myID);
			
			memset(tmp,'\0',sizeof(tmp));
			strncpy(tmp,buf+21,2);
			if(tmp[1] == ' ') tmp[1]='\0';
			ncID=atoi(tmp);
			//printf("ncID=%d\n",ncID);
			
			//memset(tmp,'\0',sizeof(tmp));
			//strncpy(tmp,buf+57,2);
			//if(tmp[1] == ' ') tmp[1]='\0';
			//bkID=atoi(tmp);
			
		}
	}
	pclose(fp);
	
#if 1 /*openwrt format*/
	for(i=0;i<count;i++){
		if(nodeInfo[i]->nodeId!=myID) {
			printf("<tr class=\"cbi-section-table-titles\">");
			printf("<td class=\"cbi-section-table-cell\">");
			printf("%d",nodeInfo[i]->nodeId);
			if(nodeInfo[i]->nodeId==ncID)
				printf("(NC)");
			printf("</td>");
			printf("<td class=\"cbi-section-table-cell\">");
			printf("%s",nodeInfo[i]->macaddr);
			printf("</td>");
			printf("<td class=\"cbi-section-table-cell\">");
			printf("%s",nodeInfo[i]->mocaVer);
			printf("</td>");
			printf("<td>");

			if(nodeInfo[i]->txRate2 > 0) {
				printf("CH0: %d",nodeInfo[i]->txRate1/1000000);
				printf("<br/>");
				printf("CH1: %d",nodeInfo[i]->txRate2/1000000);
				if(nodeInfo[i]->txRate3 > 0){
					printf("<br/>");
					printf("CH2: %d",nodeInfo[i]->txRate3/1000000);
					printf("<br/>");
					printf("CH3: %d",nodeInfo[i]->txRate4/1000000);
					printf("<br/>");
					printf("CH4: %d",nodeInfo[i]->txRate5/1000000);
					printf("<br/>");
				}
			} else {
				printf("%d",nodeInfo[i]->txRate1/1000000);
			}

			printf("</td>");
			printf("<td class=\"cbi-section-table-cell\">");

			if(nodeInfo[i]->txRate2 > 0) {
				printf("CH0: %d",nodeInfo[i]->rxRate1/1000000);
				printf("<br/>");
				printf("CH1: %d",nodeInfo[i]->rxRate2/1000000);
				if(nodeInfo[i]->txRate3 > 0){
					printf("<br/>");
					printf("CH2: %d",nodeInfo[i]->rxRate3/1000000);
					printf("<br/>");
					printf("CH3: %d",nodeInfo[i]->rxRate4/1000000);
					printf("<br/>");
					printf("CH4: %d",nodeInfo[i]->rxRate5/1000000);
					printf("<br/>");
				}
			} else {
				printf("%d",nodeInfo[i]->rxRate1/1000000);
			}

			printf("</td>");
			printf("<td class=\"cbi-section-table-cell\">");
			printf("%d",nodeInfo[i]->rxPWR);
			printf("</td>");
			printf("<td class=\"cbi-section-table-cell\">");
			if(nodeInfo[i]->rxFrame != 0)
				per=(nodeInfo[i]->rxFrameErr*100)/nodeInfo[i]->rxFrame;
			else
				per=0;
			printf("%.2f",per);
			printf("</td>");		
			printf("</tr>\n");
		}
	}

#else
	for(i=0;i<count;i++){
		if(nodeInfo[i]->nodeId!=myID) {
			printf("<tr>");
			printf("<td>");
			printf("%d",nodeInfo[i]->nodeId);
			if(nodeInfo[i]->nodeId==ncID)
				printf("(NC)");
			printf("</td>");
			printf("<td>");
			printf("%s",nodeInfo[i]->macaddr);
			printf("</td>");
			printf("<td>");
			printf("%s",nodeInfo[i]->mocaVer);
			printf("</td>");
			printf("<td>");

			if(nodeInfo[i]->txRate2 > 0) {
				printf("CH0: %d",nodeInfo[i]->txRate1/1000000);
				printf("<BR>");
				printf("CH1: %d",nodeInfo[i]->txRate2/1000000);
				if(nodeInfo[i]->txRate3 > 0){
					printf("<BR>");
					printf("CH2: %d",nodeInfo[i]->txRate3/1000000);
					printf("<BR>");
					printf("CH3: %d",nodeInfo[i]->txRate4/1000000);
					printf("<BR>");
					printf("CH4: %d",nodeInfo[i]->txRate5/1000000);
				}
			} else {
				printf("%d",nodeInfo[i]->txRate1/1000000);
			}

			printf("</td>");
			printf("<td>");

			if(nodeInfo[i]->txRate2 > 0) {
				printf("CH0: %d",nodeInfo[i]->rxRate1/1000000);
				printf("<BR>");
				printf("CH1: %d",nodeInfo[i]->rxRate2/1000000);
				if(nodeInfo[i]->txRate3 > 0){
					printf("<BR>");
					printf("CH2: %d",nodeInfo[i]->rxRate3/1000000);
					printf("<BR>");
					printf("CH3: %d",nodeInfo[i]->rxRate4/1000000);
					printf("<BR>");
					printf("CH4: %d",nodeInfo[i]->rxRate5/1000000);
				}
			} else {
				printf("%d",nodeInfo[i]->rxRate1/1000000);
			}

			printf("</td>");
			printf("<td>");
			printf("%d",nodeInfo[i]->rxPWR);
			printf("</td>");
			printf("<td>");
			if(nodeInfo[i]->rxFrame != 0)
				per=(nodeInfo[i]->rxFrameErr*100)/nodeInfo[i]->rxFrame;
			else
				per=0;
			printf("%.2f",per);
			printf("</td>");		
			printf("</tr>\n");
		}
	}
#endif
	
rate_fail:
	
	for(i=0;i<16;i++){
		free(nodeInfo[i]);
	}
	free(nodeInfo);
}


