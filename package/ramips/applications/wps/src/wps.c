#include <fcntl.h>
#include <signal.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdio.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <linux/autoconf.h>
#include "wps.h"
#include "ralink_gpio.h"



#define GPIO_DEV "/dev/gpio"

enum
{
    gpio_in_wps,
    gpio_out_wps,
};


#if 1
#define DBG(...) fprintf(stderr, "<WPS> %s(),L%d: ", __FUNCTION__, __LINE__);printf(__VA_ARGS__)
#else
#define DBG(...)
#endif

int getWscStatus(char *interface)
{
	int socket_id;
	struct iwreq wrq;
	int data = 0;
	socket_id = socket(AF_INET, SOCK_DGRAM, 0);
	strcpy(wrq.ifr_ifrn.ifrn_name, interface);
	wrq.u.data.length = sizeof(data);
	wrq.u.data.pointer = (caddr_t) &data;
	wrq.u.data.flags = RT_OID_WSC_QUERY_STATUS;
	if( ioctl(socket_id, RT_PRIV_IOCTL, &wrq) == -1)
		printf("hsiwei ioctl error\n");
	close(socket_id);
	return data;
}
int gpio_set_dir_gpio(int dir, int gpio_num)
{
	int fd, req;

	fd = open(GPIO_DEV, O_RDONLY);
	if (fd < 0) {
		DBG(GPIO_DEV);
		return -1;
	}

	if (gpio_num >= 32 && gpio_num <=63){
		gpio_num = gpio_num - 32; 
		if (dir == gpio_in_wps)
			req = RALINK_GPIO6332_SET_DIR_IN;
		else
			req = RALINK_GPIO6332_SET_DIR_OUT;
	}else if(gpio_num >=64 && gpio_num<=95){
		gpio_num = gpio_num - 64; 
		if (dir == gpio_in_wps)
			req = RALINK_GPIO9564_SET_DIR_IN;
		else
			req = RALINK_GPIO9564_SET_DIR_OUT;
	}
	else
	{
        if (dir == gpio_in_wps)
	       req = RALINK_GPIO_SET_DIR_IN;
	    else
	       req = RALINK_GPIO_SET_DIR_OUT;
	}
	if (ioctl(fd, req, (0xffffffff) & (1 << gpio_num) ) < 0) {
		DBG("ioctl");
		close(fd);
		return -1;
	}
	close(fd);
	return 0;
}
int gpio_enb_irq_gpio(void)
{
	int fd;

	fd = open(GPIO_DEV, O_RDONLY);
	if (fd < 0) {
		DBG(GPIO_DEV);
		return -1;
	}
	if (ioctl(fd, RALINK_GPIO_ENABLE_INTP) < 0) {
		DBG("ioctl");
		close(fd);
		return -1;
	}
	close(fd);
	return 0;
}
int gpio_reg_info_gpio(int gpio_num)
{
	int fd;
	ralink_gpio_reg_info info;

	fd = open(GPIO_DEV, O_RDONLY);
	if (fd < 0) {
		DBG(GPIO_DEV);
		return -1;
	}
	info.pid = getpid();
	info.irq = gpio_num;
	if (ioctl(fd, RALINK_GPIO_REG_IRQ, &info) < 0) {
		DBG("ioctl");
		close(fd);
		return -1;
	}
	close(fd);
	return 0;
}
int initGpio(int gpionum)
{   
    gpio_set_dir_gpio(gpio_out_wps, gpionum);
	gpio_set_dir_gpio(gpio_in_wps, gpionum);
	gpio_enb_irq_gpio();
	gpio_reg_info_gpio(gpionum);
	return 1;
}

int main(int argc, char** argv)
{
	char ifname[16];
	sprintf(ifname,"%s","ra0");
	int new_status=0,old_status=0;
	new_status=getWscStatus(ifname);
	old_status = new_status;
	DBG("WPS init status -->%d,%d\n",old_status,new_status);
	
	if((new_status == STATUS_WSC_IDLE) || (new_status == STATUS_WSC_CONFIGURED))
	{
		system("iwpriv ra0 set WscConfMode=7");
		system("iwpriv ra0 set WscMode=2");
		system("iwpriv ra0 set WscGetConf=1");
	}
	else if(new_status == STATUS_WSC_LINK_UP)
	{
		system("iwpriv ra0 set WscStop=1");
		system("iwpriv ra0 set WscConfMode=7");
	}
	
	while (1)
	{
		new_status= getWscStatus(ifname);
		usleep(1000);
		if(new_status==old_status)
			continue;
		old_status = new_status;
		DBG("WPS status change-->%d,%d\n",old_status,new_status);

		if(old_status == STATUS_WSC_LINK_UP)
		{
            system("gpio l 4 5 5 1 0 200");    // slow blink 200 seconds
		}
		else if(new_status == STATUS_WSC_IDLE)
		{
			system("gpio l 4 0 1 0 0 0");   // turn off
			break;
		}
		else if(old_status == STATUS_WSC_IDLE)
		{
			system("gpio l 4 0 1 0 0 0");   // turn off
			break;
		}
		else if(old_status == STATUS_WSC_CONFIGURED) 
		{
			system("gpio l 4 1200 1 0 0 1");   // turn on about 120 seconds
			break;
		}
        else if(old_status == STATUS_WSC_FAIL || old_status == STATUS_WSC_PBC_SESSION_OVERLAP) 
		{
			system("gpio l 4 1 1 1 0 200");   // fast blink 40 seconds
			break;
		}
	}
}


