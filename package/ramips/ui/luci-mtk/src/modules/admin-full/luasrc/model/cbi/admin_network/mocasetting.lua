--[[
LuCI - Lua Configuration Interface

Copyright 2008 Steven Barth <steven@midlink.org>
Copyright 2010 Jo-Philipp Wich <xm@subsignal.org>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

$Id$
]]--

require("luci.sys")
require("luci.util")
m = Map("moca", translate("Setting"),translate("Here you can configure the basic functions of MoCA device."))

s = m:section(NamedSection, "setting", nil)
s.anonymous = true
s.addremove = false

o = s:option(Flag, "networksearch", translate("Network Search"))
o.rmempty = false

o = s:option(ListValue, "lof", translate("Channel"))
o.rmempty = false
o:value(1600,  translate("22(1600MHz)"))
o:value(1550,  translate("23(1550MHz)"))
o:value(1500,  translate("24(1500MHz)"))
o:value(1450,  translate("25(1450MHz)"))
o:value(1400,  translate("26(1400MHz)"))
o:value(1350,  translate("27(1350MHz)"))
o:value(1300,  translate("28(1300MHz)"))
o:value(1250,  translate("29(1250MHz)"))
o:value(1200,  translate("30(1200MHz)"))
o:value(1150, translate("31(1150MHz)"))



o = s:option(ListValue, "beacontxpower", translate("Beacon Power"))
o.rmempty = false
o:value(7,  "7")
o:value(8,  "8")
o:value(9,  "9")
o:value(10, "10")




o = s:option(Flag, "securitymodebandd", translate("Network Security"))
o.rmempty = false

pw1 = s:option(Value, "mocapasswordbandd", translate("Network Password"))
pw1.password = true
pw1:depends("securitymodebandd", "1")
pw1.datatype = "maxlength(17)"


o = s:option(Flag, "preferrednc", translate("Preferred  NC"))
o.rmempty = false



local apply = luci.http.formvalue("cbi.apply")
if apply then
	os.execute("moca.sh reload &")
    
end



return m
