MOCA_SOURCE_MXL371X:=mxl371x_pcie_v1113.tar.gz
FILE_MX1371X_EXIT = $(shell if [ -a $(DL_DIR)/$(MOCA_SOURCE_MXL371X) ];then echo "exist";else echo "notexit";fi;) 

define moca-prepare

ifeq ($(FILE_MX1371X_EXIT),exist)
	@echo =======MX1371X exist=======
	@echo "Extract MX1371X moca driver";
	$(TAR) -xvzf $(DL_DIR)/$(MOCA_SOURCE_MXL371X) -C $(LINUX_DIR)/arch/mips/;
endif

endef
